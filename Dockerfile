FROM fedora:latest
MAINTAINER "Alexander von Gluck IV <kallisti5@unixzen.com>"

RUN dnf -y update && dnf clean all
RUN dnf -y group install 'Development Tools' && dnf clean all
RUN dnf -y install gcc-arm-linux-gnu mingw32-gcc mingw64-gcc mingw-binutils-generic mingw32-binutils mingw64-binutils mingw64-winpthreads-static openssl-devel && dnf clean all
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y

ENV PATH=~/.cargo/bin:$PATH
